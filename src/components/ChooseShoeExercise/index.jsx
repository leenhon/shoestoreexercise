import React, { Component } from "react";
import ShoeInfo from "./ShoeInfo";
import ShoeList from "./ShoeList";

export default class ShoesStore extends Component {
  state = {
    shoeCur: {
      id: 1,
      name: "Adidas Prophere",
      alias: "adidas-prophere",
      price: 350,
      description:
        "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
      shortDescription:
        "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
      quantity: 995,
      image: "http://svcy3.myclass.vn/images/adidas-prophere.png",
    },
  };

  handleChangeShoe = (newShoe) => {
    this.setState( {
        shoeCur: newShoe,
    })
  };

  render() {
    return (
      <section className="products">
        <header className="headProduct">
          <div className="container">
            <h1>Shoes Shop</h1>
          </div>
        </header>
        <main className="mainProduct">
          <div className="container">
            <ShoeInfo shoeChoose={this.state.shoeCur}></ShoeInfo>
            <ShoeList handleChangeShoe={this.handleChangeShoe}></ShoeList>
          </div>
        </main>
      </section>
    );
  }
}
